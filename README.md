# E-commerce Scaffold

An E-commerce template


### Getting Started ###
- - - -

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


#### Prerequisites ####
- - - -

* npm: 6.01

* Python 2


#### Installation ####
- - - -

[Optional: Creating a Virtual Environment]

* For **Ubuntu/Mac OS X**

    ```sudo pip install virtualenvwrapper```
  
* For **Ubuntu** :

    ```source `which virtualenvwrapper.sh```

  
  
    To activate virtualenv
          
          
* For **Mac OS X** Locate the same file in your home directory, and add the following text at the end of your file

    ```export WORKON_HOME=$HOME/.virtualenvs```
   
    ```export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3```
   
    ```export PROJECT_HOME=$HOME/Devel```
   
    ```source /usr/local/bin/virtualenvwrapper.sh```
  
* For **Windows**:

    ```pip install virtualenvwrapper-win```
  
* Creating the virtual environment

    ```cd .```
  
    ```mkvirtualenv virtualenvname```
  
    ```virtualenv anyvirtualenvname```
    
[Cloning the Project]

* 
    ```git clone https://bumbleshet@bitbucket.org/bumbleshet/e-commerce-scaffold.git```
    
    ```cd e-commerce-scaffold```
  
  
[Installing Requirements]

* ```pip install -r requirements.txt```


[Building Assets]

* Because longclaw version is in pre-release, building it's assets is needed. This is important in order to load js files:
    
    ```{% longclaw_vendors_bundle %}```
    
    ```{% longclaw_client_bundle %} ```



* To build assets run:
    
    
    ```longclaw build```
    
    
[Running Server]

* Run migrations and create admin user
    
    ```python manage.py migrate```
    
    ```python manage.py createsuperuser```
    
    ```python manage.py runserver```


[Installation Through Docker]

* Build the docker:

    ```docker build -t longclaw-demo .```
  
    ```Start the docker container and expose port 8000:```
     
    ```docker run -it -p 8000:8000 longclaw-demo```
     
     
### Frameworks used ###

* Longclaw: https://github.com/JamesRamm/longclaw

* Wagtail: https://github.com/wagtail/wagtail/releases/tag/v1.13.1

### Running the tests ###
- - - -

* Writing tests
* Code review
* Other guidelines

### Deployment ###
- - - -

### Authors ###
- - - -
* Guaro, Sidney


  @bumbleshet on Github
