// Javascript from longclaw_demo_site
var currentVal;
$(document).ready(function(){
    // When the page loads, get the number of items in the basket
    // to display in the nav bar
    $.get({
        url: '/api/basket/count',
        success: function(data){ setCartCount(data.quantity); },
        headers: {"X-CSRFToken": Cookies.get('csrftoken')}
    });
    // Respond to checkout button click events
    $('#btn-checkout').click(checkout);

    $('#order-modal').on('hidden.bs.modal', function (event) {
        document.location.href = '/';
    });

    $('#add-billing').click(function(){
        console.log("CLICK")
        $('#billing-form').show();
        $('#add-billing').hide();
    });

    // $('#btn-add-to-basket').click(increaseItemCount, function(event){
    //     event.preventDefault();
    //     $.ajax({
    //         type: 'POST',
    //         url:'/api/basket/',
    //         data:{
    //             quantity:currentVal
    //         },
    //         success:function(){
    //             console.log(variant_id)
    //         }
            
    //     })
    // });
    $('#btn-add-to-basket').click(increaseItemCount)
});

/**
 * Set the value of the basket item counter in the nav bar
 * @param {*} count
 */
function setCartCount(count) {
    if (count === 0) {
        $('#item-count').text('');
    }
    else {
        $('#item-count').text(`${count} `);
    }
}

/**
 * Increase the nav bar item count by 1
 */
function increaseItemCount() {
    var count = parseInt($('#item-count').text());
    if (count > 0) {
        count += 1;
    }
    else {
        count = 1;
    }
    setCartCount(count);
}

/**
 * Perform a checkout and create an order.
 * Since we are using the dummy payment gateway,
 * we dont need to gather any details
 */
function checkout() {
    var fakeData = {
        address: {
            shipping_name: '',
            shipping_address_line1: '',
            shipping_address_city: '',
            shipping_address_zip: '',
            shipping_address_country: '',
            billing_name: '',
            billing_address_line1: '',
            billing_address_city: '',
            billing_address_zip: '',
            billing_address_country: ''
        },
        email: '',
        shipping_rate: 0
    };
    $.post({
        url: '/api/checkout/',
        data: JSON.stringify(fakeData),
        success: function(data){
            console.log(data);
            $('#order-id').text(data.order_id);
            $('#order-modal').modal('show');
        },
        headers: {'X-CSRFToken': Cookies.get('csrftoken'), 'Content-Type': 'application/json'}
    });
}

//Quantity Input
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
                
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });




