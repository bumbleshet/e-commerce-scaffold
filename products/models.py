from django.db import models
from django import template
from modelcluster.fields import ParentalKey
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel
from wagtail.wagtailsearch import index
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsnippets.models import register_snippet
from wagtail.wagtailsnippets.edit_handlers import SnippetChooserPanel
from wagtail.contrib.wagtailroutablepage.models import RoutablePageMixin, route

from longclaw.longclawproducts.models import ProductVariantBase, ProductBase
from longclaw.longclawbasket.utils import get_basket_items as get_basket


class ProductIndex(Page):
    """
         Model that represents the product index page.

         Otherwise known as the products's home page.
     """

    #subpage_types = ('products.Product', 'products.ProductCategoryIndex')
    description = RichTextField()

    content_panels = Page.content_panels + [
        FieldPanel('description')
    ]

    @route(r'^categories/$', name='category_archive')
    @route(r'^categories/([\w-]+)/$', name='category_archive')
    def category_archive(self, request, tag=None):
        print("hello")
        posts = self.get_posts_snippets(tag)
        context = {
            'tag': tag,
            'posts': posts
        }
        return render(request, 'products/product_category_index.html', context)


class Product(RoutablePageMixin, ProductBase):
    description = RichTextField()


    content_panels = ProductBase.content_panels + [
        InlinePanel('images', label="Gallery images", max_num=6),
        InlinePanel('variants', label='Product variant', max_num=1),
        FieldPanel('description'),
        InlinePanel(
            'product_category_relationship', label="Category(ies)",
            panels=None),
        InlinePanel(
            'product_brand_relationship', label="Brand",
            panels=None, max_num=1),
    ]

    def get_brand(self):
        brand = self.product_brand_relationship.first().brand
        return brand
    
    @property
    def get_categories(self):
        """
            Returns the Products's related Categories.

            The ParentalKey's related_name from the ProductCategoryRelationship
            model is used to access these objects.

            This allows us to access the Category objects with a
            loop on the template.
        """
    
        categories = [
            n.category for n in self.product_category_relationship.all()
        ]
        for category in categories:

            category.url = '/'+'/'.join(s.strip('/') for s in [
                self.get_parent().url,
                'categories',
                category.title
            ])
        return categories
    
    

    # Returns the child BlogPage objects for this BlogPageIndex.
    # If a tag is used then it will filter the posts by tag.
    def get_posts_snippets(self, tag=None):
        posts = Product.objects.live().descendant_of(self)
        if tag:
            posts = Categories.objects.all().filter(title=tag).first()
            posts=posts.get_usage()
            self.print_title(posts)
            posts=Product.objects.filter(title__in=self.print_title(posts))
            
        return posts
    
    def print_title(self, pages):
        l=[]
        for page in pages:
            l.append(page.title)
        return l

class ProductVariant(ProductVariantBase):
    """
      Enter your custom product variant fields here
      e.g. colour, size, stock and so on.
      Remember, ProductVariantBase provides 'price', 'ref', 'slug' fields
      and the parental key to the Product model.
    """
    product = ParentalKey(Product, related_name='variants')


@register_snippet
class Categories(models.Model):
    """
        A Django model to define the product type
        It uses the `@register_snippet` decorator to allow it to be accessible
        via the Snippets UI. In the Product model you'll see we use a ForeignKey
        to create the relationship between Categories and Product. This allows a
        many-to-many relationship.
    """

    title = models.CharField(max_length=255)

    panels = [
        FieldPanel('title'),
    ]

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Product Category"
        verbose_name_plural = "Product Categories"


@register_snippet
class Brand(models.Model):
    title = models.CharField(max_length=255)

    panels = [
        FieldPanel('title'),
    ]

    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = "Product Brand"


class ProductCategoryRelationship(Orderable, models.Model):
    """
        This allows Category to be added to a Product.

        We have created a two way relationship between Product and Category using
        the ParentalKey and ForeignKey.
    """
    product = ParentalKey(
        'Product', related_name='product_category_relationship', on_delete=models.CASCADE
    )
    category = models.ForeignKey(
        'Categories', related_name='category_product_relationship', on_delete=models.CASCADE
    )
    panels = [
        SnippetChooserPanel('category')
    ]


class ProductBrandRelationship(Orderable, models.Model):
    product = ParentalKey(
        'Product', related_name='product_brand_relationship', on_delete=models.CASCADE
    )
    brand = models.ForeignKey(
        'Brand', related_name='brand_product_relationship', on_delete=models.CASCADE
    )
    panels = [
        SnippetChooserPanel('brand')
    ]


class ProductImage(Orderable):
    """
        This allows image to be added to products through 'image'.
    """
    product = ParentalKey(Product, related_name='images')
    image = models.ForeignKey('wagtailimages.Image', on_delete=models.CASCADE, related_name='+')
    caption = models.CharField(blank=True, max_length=255)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption')
    ]


