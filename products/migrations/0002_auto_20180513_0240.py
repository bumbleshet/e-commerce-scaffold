# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-05-13 02:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productimage',
            name='image',
        ),
        migrations.RemoveField(
            model_name='productimage',
            name='product',
        ),
        migrations.RemoveField(
            model_name='productvariant',
            name='description',
        ),
        migrations.RemoveField(
            model_name='productvariant',
            name='slug',
        ),
        migrations.AddField(
            model_name='productindex',
            name='description',
            field=wagtail.wagtailcore.fields.RichTextField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='productvariant',
            name='music_format',
            field=models.CharField(choices=[(b'CD', b'CD'), (b'Vinyl', b'Vinyl')], default=1, max_length=10),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='ProductImage',
        ),
    ]
