from django import template
from products.models import Categories
from products.models import Product
from wagtail.contrib.wagtailroutablepage.models import RoutablePageMixin, route

register = template.Library()


@register.simple_tag
def get_categories():
    """Get all Categories for base.html display."""
    categories=Categories.objects.all()

    for category in categories:
        category.url = '/'+'categories/'+category.title
    return categories

